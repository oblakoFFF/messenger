from flask import Flask


app = Flask(__name__)

from .modules.auth.controller import auth
app.register_blueprint(auth)

from .modules.profile.controller import profile
app.register_blueprint(profile)

if __name__ == '__main__':
    app.run('0.0.0.0', 5000, True, True)
