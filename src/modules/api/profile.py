from . import api
from .methods.get import db_getProfileInfo


def isUserBlocked(ID):
    return db_getProfileInfo(ID)["is_blocked"]

def isUserDeleted(ID):
    return db_getProfileInfo(ID)["is_deleted"]

# TODO
def isUserAuthorized():
    return False