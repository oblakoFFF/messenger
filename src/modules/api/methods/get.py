from modules.api.methods.sql_execute import sql_execute


def db_getProfileInfo(ID):
    query = """
        SELECT
            first_name
            , second_name
            , id
            , last_visit
            , is_deleted
            , is_blocked
            , is_confirmed
        FROM 
            users
        WHERE
            id={:d}
    """.format(ID)
    return sql_execute(query, False)
