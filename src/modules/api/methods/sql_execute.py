from flask import Blueprint
import psycopg2
from psycopg2.extras import RealDictCursor

api = Blueprint('api', __name__)


def sql_execute(query, fetchAll=True):
    conn = psycopg2.connect(dbname='messenger_1', user='messenger_1', password='messenger_1', host='90.189.168.29')
    cursor = conn.cursor(cursor_factory=RealDictCursor)

    records = None
    cursor.execute(query)
    try:
        records = cursor.fetchall() if fetchAll else cursor.fetchone()
    except psycopg2.Error as err:
        print(err)
        conn.rollback()
    finally:
        cursor.close()
        conn.close()
        del cursor, conn
        return records
